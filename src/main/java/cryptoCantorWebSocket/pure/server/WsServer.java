package cryptoCantorWebSocket.pure.server;

import cryptoCantorWebSocket.pure.BasicAuth;
import bittech.lib.utils.Config;
import bittech.lib.utils.Require;
import bittech.lib.utils.Utils;
import bittech.lib.utils.exceptions.StoredException;
import bittech.lib.utils.logs.Log;
import io.undertow.Undertow;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import io.undertow.websockets.spi.WebSocketHttpExchange;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static io.undertow.Handlers.path;
import static io.undertow.Handlers.websocket;

public class WsServer implements AutoCloseable {

    private final WsServerConfig config;

    private final Undertow server;

    private BiConsumer<String, String> msgConsumer = null;

    private final SrvUsers srvUsers = new SrvUsers();

    public static WsServer fromConfig() {
        try {
            WsServerConfig config = Config.getInstance().getEntry("wsServer", WsServerConfig.class);
            config.verify();
            return new WsServer(config);
        } catch (Exception ex) {
            throw new StoredException("Failed to create no-ssl undertow ws server", ex);
        }
    }

    public WsServer(WsServerConfig config) {
        try {
            System.out.println("Createing undertow ws server (no ssl) on port " + config.getPort());
            this.config = Require.notNull(config, "config");

            Undertow.Builder builder = Undertow.builder();
            builder.addHttpListener(config.getPort(), "0.0.0.0");
            addHandler(builder);
            server = builder.build();
            System.out.println("Undertow server on port " + config.getPort() + " created");

        } catch (Exception ex) {
            throw new StoredException("Failed to create no-ssl undertow ws server", ex);
        }
    }


//    private WsServer(int port) {
//        try {
//            System.out.println("Createing undertow ws server (no ssl) on port " + port);
//            this.port = Require.inRange(port, 1, Short.MAX_VALUE, "port");
//
//            Undertow.Builder builder = Undertow.builder();
//            builder.addHttpListener(port, "0.0.0.0");
//            addHandler(builder);
//            server = builder.build();
//            System.out.println("Undertow server on port " + port + " created");
//
//        } catch (Exception ex) {
//            throw new StoredException("Failed to create undertow ws server with ssl", ex);
//        }
//    }

//    private WsServer(String sslPwd, String sslFileName) {
//        try {
//            System.out.println("Createing undertow ws server with ssl");
//            Require.notEmpty(sslPwd, "sslPwd");
//            Require.notEmpty(sslFileName, "sslFileName");
//            this.port = 443;
////            transformer = addTransformer();
////            apiModule = new WebSocketServer();
//            Undertow.Builder builder = Undertow.builder();
//            SSLContext sslContext = KeystoreLoader
//                    .createSSLContext(KeystoreLoader.loadKeyStore(sslFileName, sslPwd), sslPwd);
//            builder.addHttpsListener(port, "0.0.0.0", sslContext);
//            addHandler(builder);
//            server = builder.build();
//            System.out.println("Undertow server created");
//        } catch (Exception ex) {
//            throw new StoredException("Failed to create undertow ws server with ssl", ex);
//        }
//    }

    public void start() {
        try {
            if(msgConsumer == null) {
                throw new Exception("No message consumer. You have to call WsServer.onMsgRecv before start");
            }
            server.start();
        } catch (Exception ex) {
            throw new StoredException("Failed to start server on port " + config.getPort(), ex);
        }
    }

    private void addHandler(Undertow.Builder builder) {
        builder.setHandler(path().addPrefixPath("/api", websocket(this::onConnect)));
    }

    @Override
    public void close() {
        Log.build().param ("prj", "lib-ws").event("Stopping ws server...");
        if (server != null) {
            server.stop();
        }
        System.out.println("Done");
    }

    public void onMsgRecv(BiConsumer<String, String> msgConsumer) {
        this.msgConsumer = Require.notNull(msgConsumer, "msgConsumer");
    }

    public WsServerUser findUserMatches(String message) {
        WsServerUser ret = null;
        String lastRegex = null;
        for(WsServerUser usr : config.getUsers()) {
            if(usr.autosend == null) {
                continue;
            }
            for(String regex : usr.autosend) {
                if(message.matches(regex)) {
                    if(lastRegex != null) {
                        throw new StoredException("Message to autosend matches more than single regex. It matches '" + lastRegex + "' as well as '" + regex + "'. Message:  " + message, null);
                    }
                    ret = usr;
                    lastRegex = regex;
                }
            }
        }
        return ret;
    }

    public synchronized boolean hasUser(String login) {
        return getByUsrLogin(login) != null;
    }

    public synchronized WsServerUser getByUsrLogin(String login) {
        List<WsServerUser> list = config.getUsers().stream().filter((usr) -> usr.login.equals(login)).collect(Collectors.toList());
        if (list.size() > 1) {
            throw new StoredException("More then single user logged in with login: " + login, null);
        } else if (list.size() == 1) {
            return list.get(0);
        } else {
            return null;
        }
    }

    private void onConnect(WebSocketHttpExchange exchange, WebSocketChannel channel) {
        String authHeader = exchange.getRequestHeader("Authorization");

        BasicAuth auth;
        if(authHeader == null) {
            auth = BasicAuth.fromLoginData("*", "*");
        } else {
            auth = BasicAuth.fromHeader(authHeader);
        }

        WsServerUser usr = authenticate(auth.getLogin(), auth.getPwd());

        WebSocketChannel oldChnnel = srvUsers.addOrReplace(usr, channel);

        channel.addCloseTask((ch) -> srvUsers.removeByChannel(ch));

        channel.getReceiveSetter().set(new AbstractReceiveListener() {

            @Override
            protected void onFullTextMessage(WebSocketChannel channel, BufferedTextMessage message) {
                final String messageData = message.getData();
                String userName = srvUsers.getUserName(channel);
                System.out.println("Recv from channel " + channel.toString() +" msg " + messageData);

                msgConsumer.accept(userName, messageData);
            }
        });
        channel.resumeReceives();

        if(oldChnnel != null) {
            try {
                oldChnnel.setCloseReason("You are old");
                oldChnnel.sendClose();
            } catch(Exception ex) {
                Log.build().setSeverity(Log.Severity.Error).param("exception", ex).event("Closing old channel failed");
            }
        }
    }

    public void sendWithRetries(String usr, String msg, int retries) {
        try {
            for(int i = 0; i< retries; i++) {
                List<SrvUsers.LoggedUser> users = srvUsers.getByLoginOrCategory(usr);
                if(users.size() == 0) {
                    Utils.sleep(10);
                } else {
                    send(usr, msg);
                    return;
                }
            }
        } catch(Exception ex) {
            throw new StoredException("Failed to send with " + retries + "retries ", ex);
        }
    }

    public void send(String usr, String msg) {
        try {
            List<SrvUsers.LoggedUser> users = srvUsers.getByLoginOrCategory(usr);
            for(SrvUsers.LoggedUser loggedUsr: users) {
                System.out.println("Sending to channel " + loggedUsr.channel.toString() + " msg " + msg);
                WebSockets.sendTextBlocking(msg, loggedUsr.channel);
            }
        } catch(Exception ex) {
            throw new StoredException("Failed to send message " + msg + " to user " + usr, ex);
        }
    }

    public WsServerUser findClient(String clientId, String message) {
        if("autosend".equals(clientId)) {
            return findUserMatches(message);
        } else {
            return getByUsrLogin(clientId);
        }
//        return srvUsers.getByLoginOrCategory(clientId).size() > 0;
    }

    public WsServerUser authenticate(String login, String pwd) {
        for(WsServerUser user : config.getUsers()) {
            if(login.equals(user.login) && pwd.equals(user.pwd)) {
                return user;
            }
        }
        throw new StoredException("Failed to authenticate user: " + login, null);
    }

    public void waitForConnection(String clientId) {
        srvUsers.getChannel(clientId);
    }
}
