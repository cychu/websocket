package cryptoCantorWebSocket.pure.server;

import bittech.lib.utils.Require;

import java.util.List;

public class WsServerConfig {

    private int port;
    private List<WsServerUser> users;

    public int getPort() {
        return port;
    }

    public List<WsServerUser> getUsers() {
        return users;
    }

    public void verify() {
        Require.inRange(port, 1, Short.MAX_VALUE, "port");
    }
}
