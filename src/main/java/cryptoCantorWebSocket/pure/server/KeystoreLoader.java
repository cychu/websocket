package cryptoCantorWebSocket.pure.server;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;

public class KeystoreLoader {

	public static KeyStore loadKeyStore(String name, String password) throws Exception {
		try (InputStream is = new FileInputStream(name)) {
			KeyStore loadedKeystore = KeyStore.getInstance("JKS");
			loadedKeystore.load(is, password.toCharArray());
			return loadedKeystore;
		}
	}

	public static SSLContext createSSLContext(final KeyStore keyStore, String password) throws Exception {
		KeyManager[] keyManagers;
		KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		keyManagerFactory.init(keyStore, password.toCharArray());
		keyManagers = keyManagerFactory.getKeyManagers();

		SSLContext sslContext;
		sslContext = SSLContext.getInstance("TLS");
		sslContext.init(keyManagers, null, null);

		return sslContext;
	}

}
