package cryptoCantorWebSocket.pure.server;

import bittech.lib.utils.Require;
import bittech.lib.utils.Utils;
import bittech.lib.utils.exceptions.StoredException;
import bittech.lib.utils.logs.Log;
import io.undertow.websockets.core.WebSocketChannel;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class SrvUsers {

    class LoggedUser {
        WsServerUser usr;
        WebSocketChannel channel;

        public LoggedUser(WsServerUser usr, WebSocketChannel channel) {
            this.usr = Require.notNull(usr, "usr");
            this.channel = Require.notNull(channel, "channel");
        }
    }

    private final List<LoggedUser> loggedUsers = new LinkedList<>();

    private LoggedUser getByUsrLogin(String login) {
        List<LoggedUser> list = loggedUsers.stream().filter((loggedUser) -> loggedUser.usr.login.equals(login)).collect(Collectors.toList());
        if (list.size() > 1) {
            throw new StoredException("More then single user logged in with login: " + login, null);
        } else if (list.size() == 1) {
            return list.get(0);
        } else {
            return null;
        }
    }

    private List<LoggedUser> getByUsrNameOrCategory(String loginOrCategory) {
        List<LoggedUser> list = loggedUsers.stream().filter((loggedUser) -> (
                (loggedUser.usr.category != null &&
                loggedUser.usr.category.equals(loginOrCategory))
                        || loggedUser.usr.login.equals(loginOrCategory)))
                .collect(Collectors.toList());
        return list;
    }

    private LoggedUser getByChannel(WebSocketChannel channel) {
        List<LoggedUser> list = loggedUsers.stream().filter((loggedUser) -> loggedUser.channel == channel).collect(Collectors.toList());
        if (list.size() > 1) {
            throw new StoredException("More then single user logged in with channel: " + channel, null);
        } else if (list.size() == 1) {
            return list.get(0);
        } else {
            return null;
        }
    }

//    private final Map<String, WebSocketChannel> byUserName = new HashMap<>();
//    private final Map<WebSocketChannel, String> byChannel = new HashMap<>();

    public synchronized WebSocketChannel addOrReplace(WsServerUser usr, WebSocketChannel channel) {

        Require.notNull(channel, "channel");

        String usrName = usr.login;

        LoggedUser loggedUsr = getByUsrLogin(usrName);

        if (loggedUsr != null) {
            WebSocketChannel oldChannel = loggedUsr.channel;
            Log.build().param("prj", "lib-ws")
                    .param("usrName", usrName)
                    .param("oldChannel", oldChannel.toString())
                    .param("channel", channel.toString())
                    .event("Existing user reconnect");
            loggedUsr.channel = channel;
            return oldChannel;
        } else {
            Log.build().param("prj", "lib-ws")
                    .param("usrName", usrName)
                    .param("channel", channel.toString())
                    .event("New user connected");
            loggedUsers.add(new LoggedUser(usr, channel));
            return null; // No old channel
        }
    }

    /**
     * Receives channel if is opened for given user
     */
    public WebSocketChannel getChannel(String usrName) {
        for (int i = 0; i < 100; i++) {
            WebSocketChannel channel = tryToGetChannel(usrName);
            if (channel != null && channel.isOpen()) {
                return channel;
            }
            Utils.sleep(10);
        }
        throw new StoredException("No Web socket channel crated for user: " + usrName, null);
    }

    public synchronized WebSocketChannel tryToGetChannel(String usrLogin) {
        List<LoggedUser> usrList = getByUsrNameOrCategory(usrLogin);
        return usrList.size() >= 1 ? usrList.get(0).channel : null;
    }

//    public synchronized boolean hasUser(String usrLogin) {
//        return getByUsrLogin(usrLogin) != null;
//    }

//    public synchronized LoggedUser getByMatches(String messageStr) {
//        for(LoggedUser loggedUser : loggedUsers) {
//            if(loggedUser.)
//        }
//    }

    public synchronized List<LoggedUser> getByLoginOrCategory(String loginOrCategory) {
        return getByUsrNameOrCategory(loginOrCategory);
    }

    public String getUserName(WebSocketChannel channel) {
        for (int i = 0; i < 100; i++) {
            String userName = tryToGetUserName(channel);
            if (userName != null) {
                return userName;
            }
            Utils.sleep(10);
        }
        throw new StoredException("No userName for Web socket channel", null);
    }

    public synchronized String tryToGetUserName(WebSocketChannel channel) {
        LoggedUser usr = getByChannel(channel);
        return usr != null ? usr.usr.login : null;
    }

    public synchronized void removeByChannel(WebSocketChannel channel) {
        LoggedUser usr = getByChannel(channel);
        if(usr == null) {
            Log.build().setSeverity(Log.Severity.Warning).param("channel", channel.toString()).event("Failed to remove channel from srvUsers. No such channel");
        } else {
            loggedUsers.remove(usr);
            Log.build().param("prj", "lib-ws")
                    .param("usrLogin", usr.usr.login)
                    .param("usrCategory", usr.usr.category)
                    .param("channel", channel.toString())
                    .event("User disconnected");
        }


    }
}
