package cryptoCantorWebSocket.pure.client;

import cryptoCantorWebSocket.pure.BasicAuth;
import bittech.lib.utils.Require;
import bittech.lib.utils.Utils;
import bittech.lib.utils.exceptions.StoredException;
import bittech.lib.utils.logs.Log;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class WsClient implements AutoCloseable {

    private enum Status {
        FRESH, CONNECTED, DISCONNECTED, CLOSED
    }

    private final String peerName;
    private final String serverUri;
    private final String usr;
    private final String pwd;

    private WebSocketClient client = null;

    private BiConsumer<String, String> msgConsumer = null;

    private Status status = Status.FRESH;

    public WsClient(String peerName, String serverURI, String usr, String pwd) {
        this.peerName = Require.notEmpty(peerName, "peerName");
        this.serverUri = Require.notEmpty(serverURI, "serverURI");
        this.usr = Require.notEmpty(usr, "usr");
        this.pwd = Require.notEmpty(pwd, "pwd");
    }

    public WsClient(String peerName, String serverURI) {
        this.peerName = Require.notEmpty(peerName, "peerName");
        this.serverUri = Require.notEmpty(serverURI, "serverURI");
        this.usr = null;
        this.pwd = null;
    }

    private static Map<String, String> makeAuthHeader(String usr, String pwd) {
        Map<String, String> httpHeaders = new HashMap<>();
        if (usr != null) {
            BasicAuth auth = BasicAuth.fromLoginData(usr, pwd);
            httpHeaders.put("Authorization", auth.encodeToHeader());
        }
        return httpHeaders;
    }


    @Override
    public synchronized void close() {
        this.status = Status.CLOSED;
        if (client != null) {
            client.close();
        }
    }

    public synchronized void start() {
        try {
            if (msgConsumer == null) {
                throw new Exception("No message consumer. You have to call WsServer.onMsgRecv before start");
            }
            reconnect();
        } catch (Exception ex) {
            throw new StoredException("Failed to start socket client with serverUri = " + this.serverUri, ex);
        }
    }

    public synchronized void reconnect() {
        Map<String, String> headers = makeAuthHeader(usr, pwd);
        client = new WebSocketClient(URI.create(serverUri), new Draft_6455(), headers, 1000) {

            @Override
            public void onOpen(ServerHandshake handshakedata) {
                status = Status.CONNECTED;
                Log.build().param("handshake", handshakedata).event("Web socket connected to " + serverUri);
            }

            @Override
            public void onMessage(String message) {
                Log.build().param("message", message).event("Web socket client - onMessage");
                msgConsumer.accept(peerName, message);
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                Log log = Log.build()
                        .param("prj", "lib-ws")
                        .param("code", code)
                        .param("reason", reason)
                        .param("remote", remote)
                        .setInspectNeeded(true)
                        .setSeverity(Log.Severity.Error);

                switch(status) {
                    case CLOSED:
                        log.event("Proper closed by client");
                        return;
                    case FRESH:
                        log.event("Failed to connect to " + serverUri);
                        status = Status.DISCONNECTED;
                        break;
                    case CONNECTED:
                        log.event("Connection lost with " + serverUri);
                        status = Status.DISCONNECTED;
                        break;
                    case DISCONNECTED:
                        // Nothing to do. Still disconnected
                        break;
                    default:
                        log.event("Internal error. Unrecognized status: " + status);
                        return;
                }
                scheduleConnection();

            }

            @Override
            public void onError(Exception ex) {
                // Nie logujemy Connection refused (log idzie w onClose)
                if (!"Connection refused (Connection refused)".equals(ex.getMessage())) {
                    Log.build().param("exception", ex).event("Web socket client - onError");
                }
            }
        };

        client.connect();
    }

    private synchronized Status getStatus() {
        return status;
    }

    private synchronized void sendByClient(String msg) {
        client.send(msg);
    }

    public void waitForConnection() {
        int counter = 0;
        while (getStatus() != Status.CONNECTED) {
            if (counter > 10000) {
                throw new StoredException("Web socket not connected to " + serverUri, null);
            }
            Utils.sleep(1);
            counter++;
        }
    }

    public void send(String msg) {
        try {
            waitForConnection();
            sendByClient(msg);
        } catch (Exception ex) {
            throw new StoredException("Failed to send message: " + msg, ex);
        }
    }

    public void onMsgRecv(BiConsumer<String, String> msgConsumer) {
        this.msgConsumer = Require.notNull(msgConsumer, "msgConsumer");
    }

    private void scheduleConnection() {
        Log.build().param ("prj", "lib-ws")
                .event("Schadule reconnecting");

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        reconnect();
                    }
                },
                1000
        );
    }
}
