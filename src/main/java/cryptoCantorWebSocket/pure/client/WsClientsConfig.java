package cryptoCantorWebSocket.pure.client;

import bittech.lib.utils.Require;
import bittech.lib.utils.Verifiable;
import bittech.lib.utils.exceptions.StoredException;

import java.util.LinkedList;
import java.util.List;

public class WsClientsConfig implements Verifiable {

    public List<ConnectionConfig> connections = new LinkedList<>();

    @Override
    public void verify() {
        try {
            Require.notNull(connections, "connections");
            if (connections.size() == 0) {
                throw new Exception("At least one connection needs to be specified or no 'wsClient' entry at all");
            }
            // TODO: Klienci: unikalne nazwy i loginy
        } catch(Exception ex) {
            throw new StoredException("Fail to verify ws clients entry", ex);
        }
    }

}
