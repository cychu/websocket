package cryptoCantorWebSocket.pure.client;

public class ConnectionConfig {

    public String peerName;
    public String serverUri;
    public String usr;
    public String pwd;
}
