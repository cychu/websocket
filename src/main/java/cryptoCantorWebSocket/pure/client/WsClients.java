package cryptoCantorWebSocket.pure.client;

import bittech.lib.utils.Require;
import bittech.lib.utils.Try;
import bittech.lib.utils.exceptions.StoredException;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class WsClients {

    private boolean started = false;
    private final Map<String, WsClient> clients = new HashMap<>();

    private BiConsumer<String, String> recvConsumer;

    public WsClients() {
    }

    public WsClients(WsClientsConfig config) {
        config.connections.forEach((el) -> addNewClient(el.peerName, el.serverUri, el.usr, el.pwd));
    }

    public synchronized void addNewClient(String connectToId, String uri, String login, String pwd) {
        try {
//            if (recvConsumer == null) {
//                throw new Exception("Need to specify recvConsumer first: WsClients.onMsgRecv");
//            }
            if(clients.containsKey(connectToId)) {
                throw new Exception("We already have client connected to: " + connectToId);
            }

            WsClient client = new WsClient(connectToId, uri, login, pwd);
            if(recvConsumer != null) {
                client.onMsgRecv(recvConsumer);
            }
            clients.put(connectToId, client);
            if (started) {
                client.start();
            }
        } catch (Exception ex) {
            throw new StoredException("Failed to add new client of uri " + uri + " with login " + login, ex);
        }
    }

    public synchronized void onMsgRecv(BiConsumer<String, String> recvConsumer) {
        this.recvConsumer = Require.notNull(recvConsumer, "recvConsumer");
        clients.values().forEach((el) -> el.onMsgRecv(recvConsumer));
    }

    public synchronized void start() {
        try {
            if (recvConsumer == null) {
                throw new Exception("Need to specify recvConsumer first: WsClients.onMsgRecv");
            }
            clients.values().forEach(WsClient::start);
            started = true;
        } catch (Exception ex) {
            throw new StoredException("Failed to start WsClient", ex);
        }
    }

    public boolean hasUser(String serverId) {
        return clients.containsKey(serverId);
    }

    public void send(String serverId, String msg) {
        if(!started) {
            throw new StoredException("Can't send message. Client is not started", null);
        }
        if(!clients.containsKey(serverId)) {
            throw new StoredException("No client for serverId: " + serverId, null);
        }
        clients.get(serverId).send(msg);
    }

    public void close() {
        clients.values().forEach((client) -> Try.printIfThrown(client::close));
    }

    public void waitForConnection(String clientId) {
        if(!started) {
            throw new StoredException("Can't wait for connection. Client is not started", null);
        }
        WsClient client = clients.get(clientId);
        if(client == null) {
            throw new StoredException("No such client: " + clientId, null);
        }
        client.waitForConnection();
    }

}
