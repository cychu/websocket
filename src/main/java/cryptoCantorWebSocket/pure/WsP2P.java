package cryptoCantorWebSocket.pure;

import cryptoCantorWebSocket.WsConfig;
import cryptoCantorWebSocket.pure.client.WsClients;
import cryptoCantorWebSocket.pure.server.WsServer;
import cryptoCantorWebSocket.pure.server.WsServerUser;
import bittech.lib.utils.Config;
import bittech.lib.utils.Try;
import bittech.lib.utils.Utils;
import bittech.lib.utils.exceptions.StoredException;

import java.util.function.BiConsumer;

public class WsP2P implements AutoCloseable {

    private final WsServer srv;
    private WsClients clients;

    public static WsP2P clientOnly() {
        return new WsP2P(false);
    }

    public static WsP2P canReceiveConnections() {
        return new WsP2P(true);
    }

    public static WsP2P fromConfig(WsConfig config) {
        return new WsP2P(config);
    }

    public static WsP2P fromConfig() {
        WsConfig config = Config.getInstance().getEntry("webSocket", WsConfig.class);
        config.verify();
        return new WsP2P(config);
    }

    private WsP2P(WsConfig config) {
        if(config.canReceiveConnections()) {
            srv = new WsServer(config.wsServer);
        } else {
            srv = null;
        }
        if(config.wsClient != null) {
            clients = new WsClients(config.wsClient);
        } else {
            clients = new WsClients();
        }
    }

    private WsP2P(boolean canReceiveConnections) {
        if(canReceiveConnections) {
            srv = WsServer.fromConfig();
        } else {
            srv = null;
        }
        clients = new WsClients();
    }

    public void start() {
        if(srv != null) {
            srv.start();
        }
        clients.start();
    }

    public void newConnection(String connectToId, String wsUri, String clientId, String clientPwd) {
        clients.addNewClient(connectToId, wsUri, clientId, clientPwd);
    }

    public void waitForConnectionEstablished(String clientId) {
        if(srv != null && srv.hasUser(clientId)) {
            srv.waitForConnection(clientId);
            return;
        }
        if (clients.hasUser(clientId)) {
            clients.waitForConnection(clientId);
            return;
        }
        throw new StoredException("Peer didn't connected for clientId: " + clientId, null);
    }

    public void send(String clientId, String msg) {
        try {
            WsServerUser srvUsr = srv == null?null:srv.findClient(clientId, msg);
            if(srvUsr != null) {
                srv.sendWithRetries(srvUsr.login, msg, 100);
                return;
            }

            for(int i=0; i<100; i++) {
                if(sendAttempt(clientId, msg)) {
                    return;
                } else {
                    Utils.sleep(10);
                };
            }
            throw new Exception("No such peer connected: " + clientId);
        } catch(Exception ex) {
            throw new StoredException("Failed to send message to peer: " + clientId, ex);
        }
    }

    public void autosend(String msg) {
        send("autosend", msg);
    }


    private boolean sendAttempt(String clientId, String msg) {
//        if (srv != null && srv.hasClient(clientId)) {
//            srv.send(clientId, msg);
//            return true;
//        } else
        if (clients.hasUser(clientId)) {
            clients.send(clientId, msg);
            return true;
        } else {
            return false;
        }
    }

    public void onRecv(BiConsumer<String, String> recvConsumer) {
        if(srv != null) {
            srv.onMsgRecv(recvConsumer::accept);
        }
        clients.onMsgRecv(recvConsumer::accept);
    }

    public void close() {
        Try.printIfThrown( () -> clients.close());
        if(srv != null) {
            Try.printIfThrown( () -> srv.close());
        }
    }

    // TODO: Tymczasowerozwiązanie!
    public void restartClients(String destId) {
        clients.start();
    }
}
