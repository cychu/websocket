package cryptoCantorWebSocket.pure;

import bittech.lib.utils.Require;
import bittech.lib.utils.exceptions.StoredException;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class BasicAuth {

    private static final String headerPrefix = "Basic ";

    private final String login;
    private final String pwd;

    public static BasicAuth fromHeader(String header) {
        try {
            if (!header.startsWith(headerPrefix)) {
                throw new Exception("Authorization param have to start from text '" + headerPrefix + "'");
            }

            String base64Header = header.substring(headerPrefix.length());
            byte[] bytesHeader = Base64.getDecoder().decode(base64Header);
            String auth = new String(bytesHeader, StandardCharsets.UTF_8);

            String login = StringUtils.substringBefore(auth, ":");
            String pwd = StringUtils.substringAfter(auth, ":");

            return new BasicAuth(login, pwd);

        } catch(Exception ex) {
            throw new StoredException("Failed to decode login and pwd from header: " + header, ex);
        }
    }

    private BasicAuth(String login, String pwd) {
        this.login = Require.notEmpty(login, "login");
        this.pwd = Require.notEmpty(pwd, "pwd");
    }

    public static BasicAuth fromLoginData(String login, String pwd) {
        return new BasicAuth(login, pwd);
    }

    public String encodeToHeader() {
        String plain = login + ":" + pwd;
        return headerPrefix + Base64.getEncoder().encodeToString(
                plain.getBytes(StandardCharsets.UTF_8));
    }

    public String getLogin() {
        return login;
    }

    public String getPwd() {
        return pwd;
    }
}
