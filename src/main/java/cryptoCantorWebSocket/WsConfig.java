package cryptoCantorWebSocket;

import cryptoCantorWebSocket.pure.client.WsClientsConfig;
import cryptoCantorWebSocket.pure.server.WsServerConfig;
import bittech.lib.utils.Verifiable;
import bittech.lib.utils.exceptions.StoredException;

public class WsConfig implements Verifiable {

    public WsClientsConfig wsClient;
    public WsServerConfig wsServer;

    @Override
    public void verify() {
        try {
            if ((wsClient == null) && (wsServer == null)) {
                throw new Exception("At least one wsClient or wsServer have to be specified");
            }

            if (wsClient != null) {
                wsClient.verify();
            }

            if (wsServer != null) {
                wsServer.verify();
            }
        } catch(Exception ex) {
            throw new StoredException("Incorrect WsConfig", ex);
        }
    }

    public boolean canReceiveConnections() {
        return wsServer != null;
    }
}
