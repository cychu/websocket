package cryptoCantorWebSocket;

import cryptoCantorWebSocket.pure.WsP2P;
import bdsystems.lib.cmd.Listener;
import bdsystems.lib.cmd.CmdProcessor;
import bdsystems.lib.cmd.commands.Command;
import bittech.lib.utils.Require;
import bittech.lib.utils.Try;
import bittech.lib.utils.exceptions.StoredException;
import bittech.lib.utils.logs.Log;

public class WsCmd implements AutoCloseable {

    private final WsP2P ws;
    private final CmdProcessor cmdProcessor;

    private WsCmd(WsP2P ws) {
        this.ws = Require.notNull(ws, "ws");
        cmdProcessor = new CmdProcessor();
        relateWebsocketAndTransformer();
    }

    public static WsCmd fromConfig() {
        return new WsCmd(WsP2P.fromConfig());
    }

    public static WsCmd fromConfig(WsConfig config) {
        return new WsCmd(WsP2P.fromConfig(config));
    }

    public static WsCmd clientOnly() {
        return new WsCmd(WsP2P.clientOnly());
    }

    public static WsCmd canReceiveConnections() {
        return new WsCmd(WsP2P.canReceiveConnections());
    }

    private void relateWebsocketAndTransformer() {

        // Gdy websocket clienta zauważy, że przyszła wiadomość,
        // powiadamia o tym cmd processora
        ws.onRecv((channelId, message) -> {
            cmdProcessor.dataReceived(channelId, message);
        });

        // Gdy cmd processor ma wiadomość do wysłania,
        // powiadamia o tym web socketa, który tą wiadomość wysyła
        cmdProcessor.onNewDataToSend((channelId, message) -> {
            ws.send(channelId, message);
        });
    }

    public void addListener(Listener listener) {
        cmdProcessor.addListener(listener);
    }

    public void newConnection(String connectToId, String wsUri, String clientId, String clientPwd) {
        ws.newConnection(connectToId, wsUri, clientId, clientPwd);
    }

    public void waitForConnectionEstablished(String clientId) {
        ws.waitForConnectionEstablished(clientId);
    }

    public void close() {
        Try.printIfThrown(() -> ws.close());
        Try.printIfThrown(() -> cmdProcessor.close());
    }

    public void send(String destId, Command<?, ?> command) {
        try {
            cmdProcessor.send(destId, command);
        }catch(Exception ex){
            if("Wait for response failed".equals(ex.getCause())){
                // schedule reconnect
                Log.build().param("command", command).param("destId", destId).event("Reconnect because of timeout");
                ws.restartClients(destId);
                cmdProcessor.send(destId, command);
            }
            throw new StoredException("Failed to send command " + command.type + " to " + destId, ex);
        }
    }

    public void send(Command<?, ?> command) {
        send("autosend", command);
    }

    public void start() {
        ws.start();
    }
}
