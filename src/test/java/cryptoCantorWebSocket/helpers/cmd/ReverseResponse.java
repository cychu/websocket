package cryptoCantorWebSocket.helpers.cmd;

import bdsystems.lib.cmd.commands.Response;

public class ReverseResponse implements Response {

	public String message;

	public ReverseResponse(String message) {
		this.message = message;
	}

	@Override
	public void verify() {
		// Nothing more to do
	}
}
