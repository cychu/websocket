package cryptoCantorWebSocket.helpers.cmd;

import bdsystems.lib.cmd.commands.Command;

public class ReverseCommand extends Command<ReverseRequest, ReverseResponse> {

	public static ReverseCommand createStub() {
		return new ReverseCommand("ping");
	}

	public ReverseCommand(String message) {
		this.timeout = 1000;
		this.request = new ReverseRequest();
		this.request.message = message;
	}

}
