package cryptoCantorWebSocket.helpers.cmd;

import bdsystems.lib.cmd.commands.Request;
import bittech.lib.utils.Require;

public class ReverseRequest implements Request {

	public String message;

	public ReverseRequest() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void verify() {
		Require.notNull(message, "message");		
	}

	@Override
	public String toString() {
		return message;
	}

}
