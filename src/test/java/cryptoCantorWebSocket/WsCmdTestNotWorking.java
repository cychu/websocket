package cryptoCantorWebSocket;

import cryptoCantorWebSocket.helpers.cmd.ReverseCommand;
import cryptoCantorWebSocket.utils.TstEnv;
import bittech.lib.utils.Config;
import bittech.lib.utils.exceptions.ExceptionManager;
import bittech.lib.utils.logs.Logs;
import bittech.lib.utils.storage.InjectStorage;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Ignore;

import java.math.BigDecimal;

public class WsCmdTestNotWorking extends TestCase {

    public void setUp() {
        InjectStorage storage = new InjectStorage();

        // @formatter:off
		storage.inject("config",
				"{\"entries\":{"
						+ "  \"printLogs\": true,"
						+ "  \"wsServer\": {"
						+ "    \"port\":3214,"
						+ "    \"users\":["
						+ "      {\"login\":\"Client1\", \"pwd\":\"pwd345\"},"
						+ "      {\"login\":\"Client2\", \"pwd\":\"pwd345\"}"
						+ "    ]"
						+ "  }"
						+ "}}");
		// @formatter:on

        Config.setConfig(storage.load("config", Config.class));

        ExceptionManager.getInstance().reset();
    }

    @Override
    public void tearDown() {
        Logs.getInstance().close();
        ExceptionManager.getInstance().reset();
    }

    public void testBigDecimal() {
        BigDecimal bd = new BigDecimal("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890.123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        System.out.println(bd.toPlainString());
    }

    public void testAuthenticationFailed() {
        try (WsCmd srv = WsCmd.canReceiveConnections(); WsCmd client = WsCmd.clientOnly()) {
            srv.start();
            client.start();
            client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");
        }
    }

    public void testBasic() {
        try (WsCmd srv = WsCmd.canReceiveConnections(); WsCmd client = WsCmd.clientOnly()) {

            client.addListener(TstEnv.getReverseListener());

            srv.start();
            client.start();

            client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");
            client.waitForConnectionEstablished("srv");

            ReverseCommand cmd = new ReverseCommand("PING");
            srv.send("Client1", cmd);
            Assert.assertNotNull(cmd.getResponse());
            Assert.assertEquals("GNIP", cmd.getResponse().message);
        }
    }

    //TODO:Uncomment
//    public void testKurencyjnePolaczenie() {
//        try (WsCmd srv = WsCmd.canReceiveConnections()) {
//
//            System.out.println("-------------- 01 --------------------------");
//
//            srv.addListener(TstEnv.getReverseListener());
//            srv.start();
//
//            System.out.println("-------------- 02 --------------------------");
//
//            try (WsCmd client1 = WsCmd.clientOnly()) {
//                client1.addListener(TstEnv.getReverseListener());
//                client1.start();
//                System.out.println("-------------- 03 --------------------------");
//                client1.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");
//                System.out.println("-------------- 04 --------------------------");
//                {
//                    ReverseCommand cmd = new ReverseCommand("PING");
//                    srv.send("Client1", cmd);
//                    System.out.println("-------------- 05 --------------------------");
//                    Assert.assertNotNull(cmd.getResponse());
//                    Assert.assertEquals("GNIP", cmd.getResponse().message);
//                }
//                System.out.println("-------------- 06 --------------------------");
//                try (WsCmd client2 = WsCmd.clientOnly()) {
//                    System.out.println("-------------- 07 --------------------------");
//                    client2.addListener(TstEnv.getReverseListener());
//                    client2.start();
//                    System.out.println("-------------- 08 --------------------------");
//                    client2.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");
////                    client2.waitForConnectionEstablished("srv");
//                    System.out.println("-------------- 09 --------------------------");
//                    for(int i =0; i<1; i++) {
////                        Utils.sleep(100);
//                        {
//                            System.out.println("-------------- 10 --------------------------");
//                            ReverseCommand cmd = new ReverseCommand("PING");
//                            srv.send("Client1", cmd);
//                            System.out.println("-------------- 11 --------------------------");
//                            Assert.assertNotNull(cmd.getResponse());
//                            Assert.assertEquals("GNIP", cmd.getResponse().message);
//                            System.out.println("-------------- 12 --------------------------");
//                        }
////                        Utils.sleep(10);
//                    }
//                }
//                System.out.println("-------------- 13 --------------------------");
////                Utils.sleep(10);
//                // TODO: Uncomment
//                System.out.println("---------- Wysylanie na stare polaczenie");
//                {
//                    System.out.println("-------------- 14 --------------------------");
//                    ReverseCommand cmd = new ReverseCommand("PING");
//                    srv.send("Client1", cmd);
//                    System.out.println("-------------- 15 --------------------------");
//                    Assert.assertNotNull(cmd.getResponse());
//                    Assert.assertEquals("GNIP", cmd.getResponse().message);
//                    System.out.println("-------------- 16 --------------------------");
//                }
//                System.out.println("-------------- 17 --------------------------");
//            }
//        }
//    }

    //TODO:Uncomment
//    public void testKurencyjnePolaczenieWorking() {
//        try (WsCmd srv = WsCmd.canReceiveConnections()) {
//
//            System.out.println("-------------- 01 --------------------------");
//
//            srv.addListener(TstEnv.getReverseListener());
//            srv.start();
//
//            System.out.println("-------------- 02 --------------------------");
//
//            try (WsCmd client1 = WsCmd.clientOnly()) {
//                client1.addListener(TstEnv.getReverseListener());
//                client1.start();
//                System.out.println("-------------- 03 --------------------------");
//                client1.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");
//                System.out.println("-------------- 04 --------------------------");
//                {
//                    ReverseCommand cmd = new ReverseCommand("PING");
//                    srv.send("Client1", cmd);
//
////                    LOG: Cmd sent: {"time":"2021-03-24 16:26:04.322","timeMillsec":1616599564322,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"Client1","raw":"{\"id\":\"7f6705c1-1d42-4e1f-a564-075d42dddb83\",\"name\":\"Reverse\",\"type\":\"request\",\"version\":\"2\",\"timeout\":1000,\"body\":{\"message\":\"PING\"}}","json":{"id":"7f6705c1-1d42-4e1f-a564-075d42dddb83","name":"Reverse","type":"request","version":"2","timeout":1000.0,"body":{"message":"PING"}}},"event":"Cmd sent"}
////                    LOG: Web socket connected to ws://localhost:3214/api: {"time":"2021-03-24 16:26:04.408","timeMillsec":1616599564408,"severity":"Info","inspectNeeded":false,"params":{"handshake":{"httpstatus":101,"httpstatusmessage":"Switching Protocols","content":null,"map":{"Connection":"Upgrade","Date":"Wed, 24 Mar 2021 15:26:04 GMT","Sec-WebSocket-Accept":"7ff28pVc6y29/RsTYkzNvUKccCQ\u003d","Sec-WebSocket-Location":"ws://localhost:3214/api","Upgrade":"WebSocket"}}},"event":"Web socket connected to ws://localhost:3214/api"}
////                    LOG: New user connected: {"time":"2021-03-24 16:26:04.423","timeMillsec":1616599564423,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-ws","usrName":"Client1","channel":"WebSocket13Channel peer /127.0.0.1:33360 local /127.0.0.1:3214[ No Receiver [] -- [] -- []]"},"event":"New user connected"}
////                    LOG: Web socket client - onMessage: {"time":"2021-03-24 16:26:04.437","timeMillsec":1616599564437,"severity":"Info","inspectNeeded":false,"params":{"message":"{\"id\":\"7f6705c1-1d42-4e1f-a564-075d42dddb83\",\"name\":\"Reverse\",\"type\":\"request\",\"version\":\"2\",\"timeout\":1000,\"body\":{\"message\":\"PING\"}}"},"event":"Web socket client - onMessage"}
////                    LOG: Cmd received: {"time":"2021-03-24 16:26:04.440","timeMillsec":1616599564440,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"srv","raw":"{\"id\":\"7f6705c1-1d42-4e1f-a564-075d42dddb83\",\"name\":\"Reverse\",\"type\":\"request\",\"version\":\"2\",\"timeout\":1000,\"body\":{\"message\":\"PING\"}}","json":{"id":"7f6705c1-1d42-4e1f-a564-075d42dddb83","name":"Reverse","type":"request","version":"2","timeout":1000.0,"body":{"message":"PING"}}},"event":"Cmd received"}
////                    LOG: Cmd sent: {"time":"2021-03-24 16:26:04.454","timeMillsec":1616599564454,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"srv","raw":"{\"id\":\"7f6705c1-1d42-4e1f-a564-075d42dddb83\",\"name\":\"Reverse\",\"type\":\"response\",\"version\":\"2\",\"timeout\":0,\"body\":{\"message\":\"GNIP\"}}","json":{"id":"7f6705c1-1d42-4e1f-a564-075d42dddb83","name":"Reverse","type":"response","version":"2","timeout":0.0,"body":{"message":"GNIP"}}},"event":"Cmd sent"}
////                    LOG: Cmd received: {"time":"2021-03-24 16:26:04.460","timeMillsec":1616599564460,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"Client1","raw":"{\"id\":\"7f6705c1-1d42-4e1f-a564-075d42dddb83\",\"name\":\"Reverse\",\"type\":\"response\",\"version\":\"2\",\"timeout\":0,\"body\":{\"message\":\"GNIP\"}}","json":{"id":"7f6705c1-1d42-4e1f-a564-075d42dddb83","name":"Reverse","type":"response","version":"2","timeout":0.0,"body":{"message":"GNIP"}}},"event":"Cmd received"}
//
//
//
//                    System.out.println("-------------- 05 --------------------------");
//                    Assert.assertNotNull(cmd.getResponse());
//                    Assert.assertEquals("GNIP", cmd.getResponse().message);
//                }
//                System.out.println("-------------- 06 --------------------------");
//                try (WsCmd client2 = WsCmd.clientOnly()) {
//                    System.out.println("-------------- 07 --------------------------");
//                    client2.addListener(TstEnv.getReverseListener());
//                    client2.start();
//                    System.out.println("-------------- 08 --------------------------");
//                    client2.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");
////                    client2.waitForConnectionEstablished("srv");
//                    System.out.println("-------------- 09 --------------------------");
//                    for(int i =0; i<1; i++) {
////                        Utils.sleep(100);
//                        {
//                            System.out.println("-------------- 10 --------------------------");
//                            ReverseCommand cmd = new ReverseCommand("PING");
//                            srv.send("Client1", cmd);
//
////                            LOG: Cmd sent: {"time":"2021-03-24 16:26:04.477","timeMillsec":1616599564477,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"Client1","raw":"{\"id\":\"2ed95395-6191-4af4-a540-415f8485fb57\",\"name\":\"Reverse\",\"type\":\"request\",\"version\":\"2\",\"timeout\":1000,\"body\":{\"message\":\"PING\"}}","json":{"id":"2ed95395-6191-4af4-a540-415f8485fb57","name":"Reverse","type":"request","version":"2","timeout":1000.0,"body":{"message":"PING"}}},"event":"Cmd sent"}
////                            LOG: Existing user reconnect: {"time":"2021-03-24 16:26:04.488","timeMillsec":1616599564488,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-ws","usrName":"Client1","oldChannel":"WebSocket13Channel peer /127.0.0.1:33360 local /127.0.0.1:3214[ No Receiver [] -- [] -- []]","channel":"WebSocket13Channel peer /127.0.0.1:33362 local /127.0.0.1:3214[ No Receiver [] -- [] -- []]"},"event":"Existing user reconnect"}
////                            LOG: Web socket connected to ws://localhost:3214/api: {"time":"2021-03-24 16:26:04.492","timeMillsec":1616599564492,"severity":"Info","inspectNeeded":false,"params":{"handshake":{"httpstatus":101,"httpstatusmessage":"Switching Protocols","content":null,"map":{"Connection":"Upgrade","Date":"Wed, 24 Mar 2021 15:26:04 GMT","Sec-WebSocket-Accept":"aXV2gNHwAvJ3asUPanrA4lMEq4Q\u003d","Sec-WebSocket-Location":"ws://localhost:3214/api","Upgrade":"WebSocket"}}},"event":"Web socket connected to ws://localhost:3214/api"}
////                            LOG: Connection lost with ws://localhost:3214/api: {"time":"2021-03-24 16:26:04.503","timeMillsec":1616599564503,"severity":"Error","inspectNeeded":true,"params":{"prj":"lib-ws","code":"1006","reason":"","remote":true},"event":"Connection lost with ws://localhost:3214/api"}
////                            LOG: Schadule reconnecting: {"time":"2021-03-24 16:26:04.505","timeMillsec":1616599564505,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-ws"},"event":"Schadule reconnecting"}
////                            LOG: Web socket client - onMessage: {"time":"2021-03-24 16:26:04.503","timeMillsec":1616599564503,"severity":"Info","inspectNeeded":false,"params":{"message":"{\"id\":\"2ed95395-6191-4af4-a540-415f8485fb57\",\"name\":\"Reverse\",\"type\":\"request\",\"version\":\"2\",\"timeout\":1000,\"body\":{\"message\":\"PING\"}}"},"event":"Web socket client - onMessage"}
////                            LOG: Cmd received: {"time":"2021-03-24 16:26:04.511","timeMillsec":1616599564511,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"srv","raw":"{\"id\":\"2ed95395-6191-4af4-a540-415f8485fb57\",\"name\":\"Reverse\",\"type\":\"request\",\"version\":\"2\",\"timeout\":1000,\"body\":{\"message\":\"PING\"}}","json":{"id":"2ed95395-6191-4af4-a540-415f8485fb57","name":"Reverse","type":"request","version":"2","timeout":1000.0,"body":{"message":"PING"}}},"event":"Cmd received"}
////                            LOG: Cmd sent: {"time":"2021-03-24 16:26:04.524","timeMillsec":1616599564524,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"srv","raw":"{\"id\":\"2ed95395-6191-4af4-a540-415f8485fb57\",\"name\":\"Reverse\",\"type\":\"response\",\"version\":\"2\",\"timeout\":0,\"body\":{\"message\":\"GNIP\"}}","json":{"id":"2ed95395-6191-4af4-a540-415f8485fb57","name":"Reverse","type":"response","version":"2","timeout":0.0,"body":{"message":"GNIP"}}},"event":"Cmd sent"}
////                            bittech.lib.utils.exceptions.StoredException: Failed to remove channel in srvUsers. No such channel: WebSocket13Channel peer /127.0.0.1:33360 local 0.0.0.0/0.0.0.0:3214[ No Receiver [] -- [] -- []]
////                            LOG: Cmd received: {"time":"2021-03-24 16:26:04.531","timeMillsec":1616599564531,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"Client1","raw":"{\"id\":\"2ed95395-6191-4af4-a540-415f8485fb57\",\"name\":\"Reverse\",\"type\":\"response\",\"version\":\"2\",\"timeout\":0,\"body\":{\"message\":\"GNIP\"}}","json":{"id":"2ed95395-6191-4af4-a540-415f8485fb57","name":"Reverse","type":"response","version":"2","timeout":0.0,"body":{"message":"GNIP"}}},"event":"Cmd received"}
////
//
//
//                            System.out.println("-------------- 11 --------------------------");
//                            Assert.assertNotNull(cmd.getResponse());
//                            Assert.assertEquals("GNIP", cmd.getResponse().message);
//                            System.out.println("-------------- 12 --------------------------");
//                        }
////                        Utils.sleep(10);
//                    }
//                }
//                System.out.println("-------------- 13 --------------------------");
//                Utils.sleep(100);
//                // TODO: Uncomment
//                System.out.println("---------- Wysylanie na stare polaczenie");
//                {
//                    System.out.println("-------------- 14 --------------------------");
//                    ReverseCommand cmd = new ReverseCommand("PING");
//                    srv.send("Client1", cmd);
////                    LOG: Proper closed by client: {"time":"2021-03-24 16:26:04.537","timeMillsec":1616599564537,"severity":"Error","inspectNeeded":true,"params":{"prj":"lib-ws","code":"1000","reason":"","remote":false},"event":"Proper closed by client"}
////                    LOG: User disconnected: {"time":"2021-03-24 16:26:04.540","timeMillsec":1616599564540,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-ws","usrName":"Client1","channel":"WebSocket13Channel peer /127.0.0.1:33362 local 0.0.0.0/0.0.0.0:3214[ No Receiver [] -- [] -- []]"},"event":"User disconnected"}
////                    LOG: Cmd sent: {"time":"2021-03-24 16:26:04.538","timeMillsec":1616599564538,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"Client1","raw":"{\"id\":\"98d85e13-20c2-494e-a4ca-bbb98699000b\",\"name\":\"Reverse\",\"type\":\"request\",\"version\":\"2\",\"timeout\":1000,\"body\":{\"message\":\"PING\"}}","json":{"id":"98d85e13-20c2-494e-a4ca-bbb98699000b","name":"Reverse","type":"request","version":"2","timeout":1000.0,"body":{"message":"PING"}}},"event":"Cmd sent"}
////                    bittech.lib.utils.exceptions.StoredException: Failed to remove channel in srvUsers. No such channel: WebSocket13Channel peer /127.0.0.1:33360 local 0.0.0.0/0.0.0.0:3214[ No Receiver [] -- [] -- []]
////                    LOG: New user connected: {"time":"2021-03-24 16:26:05.524","timeMillsec":1616599565524,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-ws","usrName":"Client1","channel":"WebSocket13Channel peer /127.0.0.1:33366 local /127.0.0.1:3214[ No Receiver [] -- [] -- []]"},"event":"New user connected"}
////                    LOG: Web socket connected to ws://localhost:3214/api: {"time":"2021-03-24 16:26:05.525","timeMillsec":1616599565525,"severity":"Info","inspectNeeded":false,"params":{"handshake":{"httpstatus":101,"httpstatusmessage":"Switching Protocols","content":null,"map":{"Connection":"Upgrade","Date":"Wed, 24 Mar 2021 15:26:05 GMT","Sec-WebSocket-Accept":"2Lgrnurej8nRW5Zf5x90S/lMK1Y\u003d","Sec-WebSocket-Location":"ws://localhost:3214/api","Upgrade":"WebSocket"}}},"event":"Web socket connected to ws://localhost:3214/api"}
////                    LOG: Web socket client - onMessage: {"time":"2021-03-24 16:26:05.537","timeMillsec":1616599565537,"severity":"Info","inspectNeeded":false,"params":{"message":"{\"id\":\"98d85e13-20c2-494e-a4ca-bbb98699000b\",\"name\":\"Reverse\",\"type\":\"request\",\"version\":\"2\",\"timeout\":1000,\"body\":{\"message\":\"PING\"}}"},"event":"Web socket client - onMessage"}
////                    LOG: Cmd received: {"time":"2021-03-24 16:26:05.542","timeMillsec":1616599565542,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"srv","raw":"{\"id\":\"98d85e13-20c2-494e-a4ca-bbb98699000b\",\"name\":\"Reverse\",\"type\":\"request\",\"version\":\"2\",\"timeout\":1000,\"body\":{\"message\":\"PING\"}}","json":{"id":"98d85e13-20c2-494e-a4ca-bbb98699000b","name":"Reverse","type":"request","version":"2","timeout":1000.0,"body":{"message":"PING"}}},"event":"Cmd received"}
////                    LOG: Cmd sent: {"time":"2021-03-24 16:26:05.550","timeMillsec":1616599565550,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"srv","raw":"{\"id\":\"98d85e13-20c2-494e-a4ca-bbb98699000b\",\"name\":\"Reverse\",\"type\":\"response\",\"version\":\"2\",\"timeout\":0,\"body\":{\"message\":\"GNIP\"}}","json":{"id":"98d85e13-20c2-494e-a4ca-bbb98699000b","name":"Reverse","type":"response","version":"2","timeout":0.0,"body":{"message":"GNIP"}}},"event":"Cmd sent"}
////                    LOG: Cmd received: {"time":"2021-03-24 16:26:05.555","timeMillsec":1616599565555,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-cmd","peer":"Client1","raw":"{\"id\":\"98d85e13-20c2-494e-a4ca-bbb98699000b\",\"name\":\"Reverse\",\"type\":\"response\",\"version\":\"2\",\"timeout\":0,\"body\":{\"message\":\"GNIP\"}}","json":{"id":"98d85e13-20c2-494e-a4ca-bbb98699000b","name":"Reverse","type":"response","version":"2","timeout":0.0,"body":{"message":"GNIP"}}},"event":"Cmd received"}
//
//                    System.out.println("-------------- 15 --------------------------");
//                    Assert.assertNotNull(cmd.getResponse());
//                    Assert.assertEquals("GNIP", cmd.getResponse().message);
//                    System.out.println("-------------- 16 --------------------------");
//                }
//                System.out.println("-------------- 17 --------------------------");
//
////                LOG: Failed to remove channel in srvUsers. No such channel: WebSocket13Channel peer /127.0.0.1:33360 local 0.0.0.0/0.0.0.0:3214[ No Receiver [] -- [] -- []]: {"time":"2021-03-24 16:26:05.560","timeMillsec":1616599564521,"severity":"Error","inspectNeeded":true,"params":{"ID wyjątku":"5686968314409658368","przyczyny":[],"tekst wyjątku":"Failed to remove channel in srvUsers. No such channel: WebSocket13Channel peer /127.0.0.1:33360 local 0.0.0.0/0.0.0.0:3214[ No Receiver [] -- [] -- []]","caly wyjatek":{"id":5686968314409658368,"timestamp":1616599564521,"detailMessage":"Failed to remove channel in srvUsers. No such channel: WebSocket13Channel peer /127.0.0.1:33360 local 0.0.0.0/0.0.0.0:3214[ No Receiver [] -- [] -- []]","cause":null,"stackTrace":[{"classLoaderName":"app","moduleName":null,"moduleVersion":null,"declaringClass":"bdsystems.exchange.walletprovider.lib.ws.pure.server.SrvUsers","methodName":"removeByChannel","fileName":"SrvUsers.java","lineNumber":78,"format":1},{"classLoaderName":"app","moduleName":null,"moduleVersion":null,"declaringClass":"bdsystems.exchange.walletprovider.lib.ws.pure.server.WsServer","methodName":"lambda$onConnect$0","fileName":"WsServer.java","lineNumber":129,"format":1},{"classLoaderName":"app","moduleName":null,"moduleVersion":null,"declaringClass":"org.xnio.ChannelListeners","methodName":"invokeChannelListener","fileName":"ChannelListeners.java","lineNumber":92,"format":1},{"classLoaderName":"app","moduleName":null,"moduleVersion":null,"declaringClass":"io.undertow.server.protocol.framed.AbstractFramedChannel$FrameCloseListener","methodName":"handleEvent","fileName":"AbstractFramedChannel.java","lineNumber":1075,"format":1},{"classLoaderName":"app","moduleName":null,"moduleVersion":null,"declaringClass":"io.undertow.server.protocol.framed.AbstractFramedChannel$FrameCloseListener","methodName":"handleEvent","fileName":"AbstractFramedChannel.java","lineNumber":990,"format":1},{"classLoaderName":"app","moduleName":null,"moduleVersion":null,"declaringClass":"org.xnio.ChannelListeners","methodName":"invokeChannelListener","fileName":"ChannelListeners.java","lineNumber":92,"format":1},{"classLoaderName":"app","moduleName":null,"moduleVersion":null,"declaringClass":"io.undertow.server.protocol.framed.AbstractFramedChannel$FrameCloseListener$1","methodName":"run","fileName":"AbstractFramedChannel.java","lineNumber":1001,"format":1},{"classLoaderName":"app","moduleName":null,"moduleVersion":null,"declaringClass":"io.undertow.server.protocol.framed.AbstractFramedChannel$1","methodName":"run","fileName":"AbstractFramedChannel.java","lineNumber":144,"format":1},{"classLoaderName":"app","moduleName":null,"moduleVersion":null,"declaringClass":"org.xnio.nio.WorkerThread","methodName":"safeRun","fileName":"WorkerThread.java","lineNumber":612,"format":1},{"classLoaderName":"app","moduleName":null,"moduleVersion":null,"declaringClass":"org.xnio.nio.WorkerThread","methodName":"run","fileName":"WorkerThread.java","lineNumber":479,"format":1}],"suppressedExceptions":[]}},"event":"Failed to remove channel in srvUsers. No such channel: WebSocket13Channel peer /127.0.0.1:33360 local 0.0.0.0/0.0.0.0:3214[ No Receiver [] -- [] -- []]"}
////                LOG: User disconnected: {"time":"2021-03-24 16:26:05.566","timeMillsec":1616599565566,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-ws","usrName":"Client1","channel":"WebSocket13Channel peer /127.0.0.1:33366 local 0.0.0.0/0.0.0.0:3214[ No Receiver [] -- [] -- []]"},"event":"User disconnected"}
////                LOG: Stopping ws server...: {"time":"2021-03-24 16:26:05.562","timeMillsec":1616599565562,"severity":"Info","inspectNeeded":false,"params":{"prj":"lib-ws"},"event":"Stopping ws server..."}
//
//            }
//        }
//    }



    public void testReconnectServerSendsFirst() {
        try (WsCmd srv = WsCmd.canReceiveConnections()) {

            srv.addListener(TstEnv.getReverseListener());
            srv.start();

            for (int i = 0; i < 2; i++) {
                System.out.println("------- next attemt");
//                Utils.sleep(10); // TODO: Usunać sleepa
                try (WsCmd client = WsCmd.clientOnly()) {

                    client.addListener(TstEnv.getReverseListener());

                    client.start();

                    client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");

                    {
                        ReverseCommand cmd = new ReverseCommand("PING");
                        srv.send("Client1", cmd);
                        Assert.assertNotNull(cmd.getResponse());
                        Assert.assertEquals("GNIP", cmd.getResponse().message);
                    }
                }
            }
        }
    }
@Ignore
    // TODO: Czasem failuje
    public void testReconnectClientSendsFirst() {
        try (WsCmd srv = WsCmd.canReceiveConnections()) {

            srv.addListener(TstEnv.getReverseListener());
            srv.start();

            for (int i = 0; i < 20; i++) {
                try (WsCmd client = WsCmd.clientOnly()) {
                    client.addListener(TstEnv.getReverseListener());

                    client.start();

                    client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");

                    ReverseCommand cmd = new ReverseCommand("PING");
                    client.send("srv", cmd);
                    Assert.assertNotNull(cmd.getResponse());
                    Assert.assertEquals("GNIP", cmd.getResponse().message);
                }
            }
        }
    }

    public void testNoClientConnected() {
        try (WsCmd srv = WsCmd.canReceiveConnections(); WsCmd client = WsCmd.clientOnly()) {

            client.addListener(TstEnv.getReverseListener());

            srv.start();
            client.start();

            client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");

            ReverseCommand cmd = new ReverseCommand("PING");
            try {
                srv.send("a", cmd);
                Assert.fail("Exception not thrown");
            } catch (Exception ex) {
                Assert.assertEquals("Failed to send command bdsystems.exchange.walletprovider.lib.ws.helpers.cmd.ReverseCommand to a", ex.getMessage());
                Assert.assertEquals("Failed to send message to peer: a", ex.getCause().getMessage());
                Assert.assertEquals("No such peer connected: a", ex.getCause().getCause().getMessage());
            }
        }
    }

//		LogsTester logTester = new LogsTester();
//		{
//			String log = JsonBuilder.build().toJson(logTester.consumeLog("Cmd request received"));
//			Assert.assertEquals("Info", JsonPath.read(log, "$.severity"));
//			Assert.assertEquals(false, JsonPath.read(log, "$.inspectNeeded"));
//			Assert.assertEquals(ReverseCommand.class.getCanonicalName(), JsonPath.read(log, "$.params.cmd.type"));
//			Assert.assertNull(JsonPath.read(log, "$.params.cmd.response"));
//			Assert.assertNull(JsonPath.read(log, "$.params.cmd.error"));
//			Assert.assertNotNull(JsonPath.read(log, "$.params.cmd.request"));
//			Assert.assertEquals("PING", JsonPath.read(log, "$.params.cmd.request.message"));
//		}
//
//		{
//			String log = JsonBuilder.build().toJson(logTester.consumeLog("Cmd response sent"));
//			Assert.assertEquals("Info", JsonPath.read(log, "$.severity"));
//			Assert.assertEquals(false, JsonPath.read(log, "$.inspectNeeded"));
//			Assert.assertEquals(ReverseCommand.class.getCanonicalName(), JsonPath.read(log, "$.params.cmd.type"));
//			Assert.assertNotNull(JsonPath.read(log, "$.params.cmd.response"));
//			Assert.assertNull(JsonPath.read(log, "$.params.cmd.error"));
//			Assert.assertNotNull(JsonPath.read(log, "$.params.cmd.request"));
//			Assert.assertEquals("PING", JsonPath.read(log, "$.params.cmd.request.message"));
//			Assert.assertEquals("GNIP", JsonPath.read(log, "$.params.cmd.response.message"));
//		}

//		logTester.assertNoMoreLogs();
//	}


}
