package cryptoCantorWebSocket;

import cryptoCantorWebSocket.helpers.cmd.ReverseCommand;
import cryptoCantorWebSocket.pure.client.ConnectionConfig;
import cryptoCantorWebSocket.pure.client.WsClientsConfig;
import cryptoCantorWebSocket.utils.TstEnv;
import bittech.lib.utils.Config;
import bittech.lib.utils.exceptions.ExceptionManager;
import bittech.lib.utils.logs.Logs;
import bittech.lib.utils.storage.InjectStorage;
import junit.framework.TestCase;
import org.junit.Assert;

import java.util.LinkedList;

public class WsAutosendTest extends TestCase {

    public void setUp() {
        InjectStorage storage = new InjectStorage();

        // @formatter:off
		storage.inject("config",
				"{\"entries\":{"
						+ "  \"printLogs\": true,"
						+ "  \"webSocket\": {"
                            + "  \"wsServer\": {"
                            + "    \"port\":3214,"
                            + "    \"users\":["
                            + "      {\"login\":\"Client1\", \"pwd\":\"pwd345\", " +
                        "              \"autosend\": [" +
                                        "\"abc\", \"def\"" +
                        "]" +
                        "},"
                            + "      {\"login\":\"Client2\", \"pwd\":\"pwd345\"}"
                            + "    ]"
                            + "  }"
//                            + "  \"wsClient\": {"
//                            + "    \"connections\":["
//                            + "      {\"peerName\":\"srv\", \"serverUri\":\"pwd345\", \"usr\":\"pwd345\", \"pwd\":\"pwd345\"}"
//                            + "    ]"
//                            + "  }"
						+ "  }"
						+ "}}");
		// @formatter:on

        Config.setConfig(storage.load("config", Config.class));

        ExceptionManager.getInstance().reset();
    }

    @Override
    public void tearDown() {
        Logs.getInstance().close();
        ExceptionManager.getInstance().reset();
    }

    public void testAsClient() {

        WsConfig conf = new WsConfig();
        conf.wsServer = null;
        conf.wsClient = new WsClientsConfig();
        ConnectionConfig conConf = new ConnectionConfig();
        conConf.peerName = "srv";
        conConf.serverUri = "ws://localhost:3214/api";
        conConf.usr = "Client1";
        conConf.pwd = "pwd345";
        conf.wsClient.connections = new LinkedList<>();
        conf.wsClient.connections.add(conConf);

        try (WsCmd client = WsCmd.fromConfig(conf); WsCmd srv = WsCmd.fromConfig()) {

            client.addListener(TstEnv.getReverseListener());

            srv.start();
            client.start();

            ReverseCommand cmd = new ReverseCommand("PING");
            srv.send("Client1", cmd);
            Assert.assertNotNull(cmd.getResponse());
            Assert.assertEquals("GNIP", cmd.getResponse().message);
        }
    }

}
