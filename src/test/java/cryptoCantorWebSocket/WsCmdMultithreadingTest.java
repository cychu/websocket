package cryptoCantorWebSocket;

import cryptoCantorWebSocket.helpers.cmd.ReverseCommand;
import cryptoCantorWebSocket.utils.TstEnv;
import bittech.lib.utils.Config;
import bittech.lib.utils.exceptions.ExceptionManager;
import bittech.lib.utils.logs.Logs;
import bittech.lib.utils.storage.InjectStorage;
import junit.framework.TestCase;
import org.junit.Assert;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class WsCmdMultithreadingTest extends TestCase {

	public void setUp() {
		InjectStorage storage = new InjectStorage();

		// @formatter:off
		storage.inject("config",
				"{\"entries\":{"
						+ "  \"saveLogs\": false,"
						+ "  \"printLogs\": true,"
						+ "  \"wsServer\": {"
						+ "    \"port\":3214,"
						+ "    \"users\":["
						+ "      {\"login\":\"Client1\", \"pwd\":\"pwd1\"},"
						+ "      {\"login\":\"Client2\", \"pwd\":\"pwd2\"},"
						+ "      {\"login\":\"Client3\", \"pwd\":\"pwd3\"},"
						+ "      {\"login\":\"Client4\", \"pwd\":\"pwd4\"},"
						+ "      {\"login\":\"Client5\", \"pwd\":\"pwd5\"},"
						+ "      {\"login\":\"Client6\", \"pwd\":\"pwd6\"},"
						+ "      {\"login\":\"Client7\", \"pwd\":\"pwd7\"},"
						+ "      {\"login\":\"Client8\", \"pwd\":\"pwd8\"},"
						+ "      {\"login\":\"Client9\", \"pwd\":\"pwd9\"},"
						+ "      {\"login\":\"Client10\", \"pwd\":\"pwd10\"},"
						+ "      {\"login\":\"Client11\", \"pwd\":\"pwd11\"}"
						+ "    ]"
						+ "  }"
						+ "}}");
		// @formatter:on

		Config.setConfig(storage.load("config", Config.class));

		ExceptionManager.getInstance().reset();
	}

	@Override
	public void tearDown() {
		Logs.getInstance().close();
		ExceptionManager.getInstance().reset();
	}

	public void testMassive() {
		try (WsCmd srv = WsCmd.canReceiveConnections(); WsCmd client = WsCmd.clientOnly()) {

			client.addListener(TstEnv.getReverseListener());

			srv.start();
			client.start();

			for(int i=1; i<10; i++) {
				client.newConnection("srv"+i, "ws://localhost:3214/api", "Client"+i, "pwd"+i);
				ReverseCommand cmd = new ReverseCommand("PING");
				srv.send("Client"+i, cmd);
				Assert.assertNotNull(cmd.getResponse());
				Assert.assertEquals("GNIP", cmd.getResponse().message);
			}
		}
	}

	public void testMultithread() throws ExecutionException, InterruptedException {

		ExecutorService executorService = Executors.newFixedThreadPool(10);

		try (WsCmd srv = WsCmd.canReceiveConnections(); WsCmd client = WsCmd.clientOnly()) {

			client.addListener(TstEnv.getReverseListener());

			srv.start();
			client.start();

			List<Future> futures = new LinkedList<>();

			for(int i=1; i<10; i++) {
				final int num = i;
				futures.add(executorService.submit(() -> {
					client.newConnection("srv" + num, "ws://localhost:3214/api", "Client" + num, "pwd" + num);
					ReverseCommand cmd = new ReverseCommand("PING");
					srv.send("Client" + num, cmd);
					Assert.assertNotNull(cmd.getResponse());
					Assert.assertEquals("GNIP", cmd.getResponse().message);
				}));
			}

			for(int i=1; i<10; i++) {
				futures.get(i-1).get();
			}
		}
	}

	public void testMassive2() {
		try (WsCmd srv = WsCmd.canReceiveConnections(); WsCmd client = WsCmd.clientOnly()) {

			client.addListener(TstEnv.getReverseListener());

			srv.start();
			client.start();

			client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd1");

			for(int i=0; i<1000; i++) {
				System.out.println("---- " + i);
				ReverseCommand cmd = new ReverseCommand("PING");
				srv.send("Client1", cmd);
				Assert.assertNotNull(cmd.getResponse());
				Assert.assertEquals("GNIP", cmd.getResponse().message);
			}
		}
	}

//	public void testBasic() {
//		try (WsCmd srv = WsCmd.canReceiveConnections(); WsCmd client = WsCmd.clientOnly()) {
//
//			client.addListener(TstEnv.getReverseListener());
//
//			srv.start();
//			client.start();
//
//			client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");
//
//			ReverseCommand cmd = new ReverseCommand("PING");
//			srv.send("Client1", cmd);
//			Assert.assertNotNull(cmd.getResponse());
//			Assert.assertEquals("GNIP", cmd.getResponse().message);
//		}
//	}

//		LogsTester logTester = new LogsTester();
//		{
//			String log = JsonBuilder.build().toJson(logTester.consumeLog("Cmd request received"));
//			Assert.assertEquals("Info", JsonPath.read(log, "$.severity"));
//			Assert.assertEquals(false, JsonPath.read(log, "$.inspectNeeded"));
//			Assert.assertEquals(ReverseCommand.class.getCanonicalName(), JsonPath.read(log, "$.params.cmd.type"));
//			Assert.assertNull(JsonPath.read(log, "$.params.cmd.response"));
//			Assert.assertNull(JsonPath.read(log, "$.params.cmd.error"));
//			Assert.assertNotNull(JsonPath.read(log, "$.params.cmd.request"));
//			Assert.assertEquals("PING", JsonPath.read(log, "$.params.cmd.request.message"));
//		}
//
//		{
//			String log = JsonBuilder.build().toJson(logTester.consumeLog("Cmd response sent"));
//			Assert.assertEquals("Info", JsonPath.read(log, "$.severity"));
//			Assert.assertEquals(false, JsonPath.read(log, "$.inspectNeeded"));
//			Assert.assertEquals(ReverseCommand.class.getCanonicalName(), JsonPath.read(log, "$.params.cmd.type"));
//			Assert.assertNotNull(JsonPath.read(log, "$.params.cmd.response"));
//			Assert.assertNull(JsonPath.read(log, "$.params.cmd.error"));
//			Assert.assertNotNull(JsonPath.read(log, "$.params.cmd.request"));
//			Assert.assertEquals("PING", JsonPath.read(log, "$.params.cmd.request.message"));
//			Assert.assertEquals("GNIP", JsonPath.read(log, "$.params.cmd.response.message"));
//		}

//		logTester.assertNoMoreLogs();
//	}


}
