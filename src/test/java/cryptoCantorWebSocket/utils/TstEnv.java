package cryptoCantorWebSocket.utils;

import cryptoCantorWebSocket.helpers.cmd.ReverseCommand;
import cryptoCantorWebSocket.helpers.cmd.ReverseResponse;
import cryptoCantorWebSocket.pure.server.WsServer;
import bdsystems.lib.cmd.Listener;
import bittech.lib.utils.Utils;
import bittech.lib.utils.exceptions.StoredException;
import bittech.lib.utils.logs.Log;
import bittech.lib.utils.tests.LogsTester;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import java.util.LinkedList;
import java.util.List;

public class TstEnv {

//    private WsServer server;
//    private List<WsClient> clients = new LinkedList<>();
//

    private LogsTester logsTester = new LogsTester();

    private List<String> sentData = new LinkedList<>();

    public synchronized Log consumeLog(String eventMsg) {
        return logsTester.consumeLog(eventMsg);
    };

    public void report(String info) {
        System.out.println(info);
        sentData.add(info);
    }

    public void consumeEvent(String info) {
        if(!sentData.remove(info)) {
            throw new RuntimeException("No such event: " + info);
        }
    }

    public synchronized void assertNoMoreEvents() {
        if(sentData.size() != 0) {
            System.err.println("Events that were not consumed:");
            sentData.forEach(Utils::prn);
            Assert.fail("No all events were consumed");
        }
    }

//    public void generateClients(int count) {
//        for(int i=0; i<count; i++) {
//            clients.add(new WsClient("ws://localhost:3715/api"));
//        }
//    }
//
//    public void connectAll() {
//        clients.forEach((client) -> client.start());
//    }
//
//    public void closeAll() {
//        clients.forEach((client) -> client.close());
//    }
//
    public static void runServer(int timeMillis) {
        try(WsServer sv = WsServer.fromConfig()) {
            sv.onMsgRecv((userName, msg) -> System.out.println(msg));
            sv.start();
            Utils.sleep(timeMillis);
        }
    }

    public static Listener getReverseListener() {
        return Listener.singleCmd(ReverseCommand.class, (cmd) -> {
            if (StringUtils.isEmpty(cmd.getRequest().message)) {
                throw new StoredException("No message provided", null);
            }

            StringBuilder sb = new StringBuilder();
            sb.append(cmd.getRequest().message);
            sb = sb.reverse();
            cmd.response = new ReverseResponse(sb.toString());
        });
    }

    public static Listener getTimeoutListener() {
        return Listener.singleCmd(ReverseCommand.class, (cmd) -> {
            if (StringUtils.isEmpty(cmd.getRequest().message)) {
                throw new StoredException("No message provided", null);
            }

            StringBuilder sb = new StringBuilder();
            sb.append(cmd.getRequest().message);
            sb = sb.reverse();
            cmd.response = new ReverseResponse(sb.toString());
        });
    }

}
