package cryptoCantorWebSocket;

import cryptoCantorWebSocket.helpers.cmd.ReverseCommand;
import cryptoCantorWebSocket.utils.TstEnv;
import bittech.lib.utils.Config;
import bittech.lib.utils.Utils;
import bittech.lib.utils.exceptions.ExceptionManager;
import bittech.lib.utils.logs.Logs;
import bittech.lib.utils.storage.InjectStorage;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Ignore;

import java.math.BigDecimal;

@Ignore
public class WsCmdTest extends TestCase {

    public void setUp() {
        InjectStorage storage = new InjectStorage();

        // @formatter:off
		storage.inject("config",
				"{\"entries\":{"
						+ "  \"saveLogs\": false,"
						+ "  \"printLogs\": true,"
						+ "  \"wsServer\": {"
						+ "    \"port\":3214,"
						+ "    \"users\":["
						+ "      {\"login\":\"Client1\", \"pwd\":\"pwd345\"},"
						+ "      {\"login\":\"Client2\", \"pwd\":\"pwd345\"}"
						+ "    ]"
						+ "  }"
						+ "}}");
		// @formatter:on

        Config.setConfig(storage.load("config", Config.class));

        ExceptionManager.getInstance().reset();
    }

    @Override
    public void tearDown() {
        Logs.getInstance().close();
        ExceptionManager.getInstance().reset();
    }

    public void testBigDecimal() {
        BigDecimal bd = new BigDecimal("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890.123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        System.out.println(bd.toPlainString());
    }

    public void testAuthenticationFailed() {
        try (WsCmd srv = WsCmd.canReceiveConnections(); WsCmd client = WsCmd.clientOnly()) {
            srv.start();
            client.start();
            client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");
        }
    }

    public void testBasic() {
        try (WsCmd srv = WsCmd.canReceiveConnections(); WsCmd client = WsCmd.clientOnly()) {

            client.addListener(TstEnv.getReverseListener());

            srv.start();
            client.start();

            client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");
            client.waitForConnectionEstablished("srv");

            ReverseCommand cmd = new ReverseCommand("PING");
            srv.send("Client1", cmd);
            Assert.assertNotNull(cmd.getResponse());
            Assert.assertEquals("GNIP", cmd.getResponse().message);
        }
    }

    @Ignore
    public void testKurencyjnePolaczenie() {
        try (WsCmd srv = WsCmd.canReceiveConnections()) {

            srv.addListener(TstEnv.getReverseListener());
            srv.start();

            try (WsCmd client1 = WsCmd.clientOnly()) {
                client1.addListener(TstEnv.getReverseListener());
                client1.start();

                client1.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");

                {
                    ReverseCommand cmd = new ReverseCommand("PING");
                    srv.send("Client1", cmd);
                    Assert.assertNotNull(cmd.getResponse());
                    Assert.assertEquals("GNIP", cmd.getResponse().message);
                }

                try (WsCmd client2 = WsCmd.clientOnly()) {
                    client2.addListener(TstEnv.getReverseListener());
                    client2.start();

                    client2.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");

                    for(int i =0; i<1; i++) {
                        {
                            ReverseCommand cmd = new ReverseCommand("PING");
                            srv.send("Client1", cmd);
                            Assert.assertNotNull(cmd.getResponse());
                            Assert.assertEquals("GNIP", cmd.getResponse().message);
                        }
                    }
                }

                System.out.println("---------- Wysylanie na stare polaczenie");
                try{
                    ReverseCommand cmd = new ReverseCommand("PING");
                    srv.send("Client1", cmd);
                    Assert.assertNotNull(cmd.getResponse());
                    Assert.assertEquals("GNIP", cmd.getResponse().message);
                } catch(Exception ex) {
                    System.out.println("--------- 1 failed");
                }
                {
                    ReverseCommand cmd = new ReverseCommand("PING");
                    srv.send("Client1", cmd);
                    Assert.assertNotNull(cmd.getResponse());
                    Assert.assertEquals("GNIP", cmd.getResponse().message);
                }
            }
        }
    }

    public void testTimeout() {
        try (WsCmd srv = WsCmd.canReceiveConnections()) {

        }
    }

    public void testReconnectServerSendsFirst() {
        try (WsCmd srv = WsCmd.canReceiveConnections()) {

            srv.addListener(TstEnv.getReverseListener());
            srv.start();

            for (int i = 0; i < 2; i++) {
                System.out.println("------- next attemt");
                Utils.sleep(10); // TODO: Usunać sleepa
                try (WsCmd client = WsCmd.clientOnly()) {

                    client.addListener(TstEnv.getReverseListener());

                    client.start();

                    client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");

                    {
                        ReverseCommand cmd = new ReverseCommand("PING");
                        srv.send("Client1", cmd);
                        Assert.assertNotNull(cmd.getResponse());
                        Assert.assertEquals("GNIP", cmd.getResponse().message);
                    }
                }
            }
        }
    }

    public void testReconnectClientSendsFirst() {
        try (WsCmd srv = WsCmd.canReceiveConnections()) {

            srv.addListener(TstEnv.getReverseListener());
            srv.start();

            for (int i = 0; i < 20; i++) {
                try (WsCmd client = WsCmd.clientOnly()) {
                    client.addListener(TstEnv.getReverseListener());

                    client.start();

                    client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");

                    ReverseCommand cmd = new ReverseCommand("PING");
                    client.send("srv", cmd);
                    Assert.assertNotNull(cmd.getResponse());
                    Assert.assertEquals("GNIP", cmd.getResponse().message);
                }
            }
        }
    }

    public void testNoClientConnected() {
        try (WsCmd srv = WsCmd.canReceiveConnections(); WsCmd client = WsCmd.clientOnly()) {

            client.addListener(TstEnv.getReverseListener());

            srv.start();
            client.start();

            client.newConnection("srv", "ws://localhost:3214/api", "Client1", "pwd345");

            ReverseCommand cmd = new ReverseCommand("PING");
            try {
                srv.send("a", cmd);
                Assert.fail("Exception not thrown");
            } catch (Exception ex) {
                Assert.assertEquals("Failed to send command to peer: a", ex.getMessage());
                Assert.assertEquals("No such peer connected: a", ex.getCause().getMessage());
            }
        }
    }

//		LogsTester logTester = new LogsTester();
//		{
//			String log = JsonBuilder.build().toJson(logTester.consumeLog("Cmd request received"));
//			Assert.assertEquals("Info", JsonPath.read(log, "$.severity"));
//			Assert.assertEquals(false, JsonPath.read(log, "$.inspectNeeded"));
//			Assert.assertEquals(ReverseCommand.class.getCanonicalName(), JsonPath.read(log, "$.params.cmd.type"));
//			Assert.assertNull(JsonPath.read(log, "$.params.cmd.response"));
//			Assert.assertNull(JsonPath.read(log, "$.params.cmd.error"));
//			Assert.assertNotNull(JsonPath.read(log, "$.params.cmd.request"));
//			Assert.assertEquals("PING", JsonPath.read(log, "$.params.cmd.request.message"));
//		}
//
//		{
//			String log = JsonBuilder.build().toJson(logTester.consumeLog("Cmd response sent"));
//			Assert.assertEquals("Info", JsonPath.read(log, "$.severity"));
//			Assert.assertEquals(false, JsonPath.read(log, "$.inspectNeeded"));
//			Assert.assertEquals(ReverseCommand.class.getCanonicalName(), JsonPath.read(log, "$.params.cmd.type"));
//			Assert.assertNotNull(JsonPath.read(log, "$.params.cmd.response"));
//			Assert.assertNull(JsonPath.read(log, "$.params.cmd.error"));
//			Assert.assertNotNull(JsonPath.read(log, "$.params.cmd.request"));
//			Assert.assertEquals("PING", JsonPath.read(log, "$.params.cmd.request.message"));
//			Assert.assertEquals("GNIP", JsonPath.read(log, "$.params.cmd.response.message"));
//		}

//		logTester.assertNoMoreLogs();
//	}


}
