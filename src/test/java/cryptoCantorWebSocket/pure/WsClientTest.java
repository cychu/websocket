package cryptoCantorWebSocket.pure;

import cryptoCantorWebSocket.pure.client.WsClient;
import bittech.lib.utils.Utils;
import bittech.lib.utils.exceptions.ExceptionManager;
import bittech.lib.utils.logs.Logs;
import bittech.lib.utils.tests.LogsTester;
import junit.framework.TestCase;
import org.junit.Assert;

public class WsClientTest extends TestCase {

    @Override
    public void setUp() {
        ExceptionManager.getInstance().reset();
    }

    @Override
    protected void tearDown() throws Exception {
        Logs.getInstance().close();
    }

    public void testNoMsgConsumer() {
//        LogsTester logsTester = new LogsTester();
        try(WsClient wsClient = new WsClient("srv", "ws://localhost:4317/api")) {
            try {
                wsClient.start();
                Assert.fail("Exception not thrown");
            } catch (Exception ex) {
                Assert.assertEquals("Failed to start socket client with serverUri = ws://localhost:4317/api", ex.getMessage());
                Assert.assertEquals("No message consumer. You have to call WsServer.onMsgRecv before start", ex.getCause().getMessage());
            }
        }
//        Utils.sleep(1000);
//        logsTester.assertNoMoreLogs();
    }

    public void testConnectionFailed() {
        LogsTester logsTester = new LogsTester();

        try(WsClient wsClient = new WsClient("srv", "ws://localhost:4317/api")) {
            wsClient.onMsgRecv((id, msg) -> System.out.println(id + ":" + msg));
            wsClient.start();
            Utils.sleep(2000);
        }

        Utils.sleep(1000);
        logsTester.consumeLog("Failed to connect to ws://localhost:4317/api");
        logsTester.consumeLog("Proper closed by client");
//        logsTester.assertNoMoreLogs(); TODO: Czasem failuje bo 2 razy ten sam log
    }

}
