package cryptoCantorWebSocket.pure;

import cryptoCantorWebSocket.utils.TstEnv;
import bittech.lib.utils.Config;
import bittech.lib.utils.Utils;
import bittech.lib.utils.exceptions.ExceptionManager;
import bittech.lib.utils.logs.Logs;
import bittech.lib.utils.storage.InjectStorage;
import junit.framework.TestCase;
import org.junit.Assert;

public class WsP2PTest extends TestCase {

    private TstEnv tstEnv;

    public void setUp() {
        InjectStorage storage = new InjectStorage();

        // @formatter:off
        storage.inject("config",
                "{\"entries\":{"
                        + "  \"saveLogs\": false,"
                        + "  \"printLogs\": true,"
                        + "  \"wsServer\": {"
                        + "    \"port\":2124,"
                        + "    \"users\":["
                        + "      {\"login\":\"Client1\", \"pwd\":\"pwd345\"},"
                        + "      {\"login\":\"Client2\", \"pwd\":\"pwd345\"}"
                        + "    ]"
                        + "  }"
                        + "}}");
        // @formatter:on

        Config.setConfig(storage.load("config", Config.class));

        ExceptionManager.getInstance().reset();

        tstEnv = new TstEnv();
    }

    @Override
    protected void tearDown() throws Exception {
        Logs.getInstance().close();
    }

    public void testBasic() {

        try(WsP2P srv = WsP2P.canReceiveConnections(); WsP2P client = WsP2P.clientOnly()) {

            srv.onRecv((connectionId, msg) -> tstEnv.report("srv < " + connectionId + " : " + msg));

            client.onRecv((connectionId, msg) -> tstEnv.report("client < " + connectionId + " : " + msg));

            srv.start();
            client.start();

            client.newConnection("srv", "ws://localhost:2124/api", "Client1", "pwd345");
            client.newConnection("srv2", "ws://localhost:2124/api", "Client2", "pwd345");

            srv.send("Client1", "Msg do clienta 1");

            client.send("srv", "Msg do servera 1");

            srv.send("Client2", "msg do clienta 2");

            client.send("srv", "Msg do servera 2");

            Utils.sleep(1000);

            tstEnv.consumeEvent("client < srv : Msg do clienta 1");
            tstEnv.consumeEvent("srv < Client1 : Msg do servera 1");
            tstEnv.consumeEvent("client < srv2 : msg do clienta 2");
            tstEnv.consumeEvent("srv < Client1 : Msg do servera 2");

            tstEnv.assertNoMoreEvents();
        }

    }

    public void testNoClientId() {
        try(WsP2P srv = WsP2P.canReceiveConnections(); WsP2P client = WsP2P.clientOnly()) {

            srv.onRecv((connectionId, msg) -> tstEnv.report("srv < " + connectionId + " : " + msg));

            client.onRecv((connectionId, msg) -> tstEnv.report("client < " + connectionId + " : " + msg));

            srv.start();
            client.start();

            client.newConnection("srv", "ws://localhost:2124/api", "Client1", "pwd345");

            try {
                client.send("a", "Msg");
                Assert.fail("Exception not thrown");
            } catch (Exception ex) {
                "Failed to send message to peer: a".equals(ex.getMessage());
                "No such peer connected: a".equals(ex.getCause().getMessage());
            }
        }
    }


}
