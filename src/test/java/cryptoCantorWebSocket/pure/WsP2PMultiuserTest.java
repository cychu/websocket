package cryptoCantorWebSocket.pure;

import cryptoCantorWebSocket.utils.TstEnv;
import bittech.lib.utils.Config;
import bittech.lib.utils.Utils;
import bittech.lib.utils.exceptions.ExceptionManager;
import bittech.lib.utils.logs.Logs;
import bittech.lib.utils.storage.InjectStorage;
import junit.framework.TestCase;

public class WsP2PMultiuserTest extends TestCase {

    private TstEnv tstEnv;

    public void setUp() {
        InjectStorage storage = new InjectStorage();

        // @formatter:off
        storage.inject("config",
                "{\"entries\":{"
                        + "  \"saveLogs\": false,"
                        + "  \"printLogs\": true,"
                        + "  \"wsServer\": {"
                        + "    \"port\":2124,"
                        + "    \"users\":["
                        + "      {\"login\":\"Client1\", \"pwd\":\"pwd345\","
                        + "         \"autosend\":[\".*catMain.*\", \".*Jeszcze cos innego.*\"]"
                        + "      },"
                        + "      {\"login\":\"Client2\", \"pwd\":\"pwd345\","
                        + "         \"autosend\":[\".*Message.*\"]"
                        + "      }"
                        + "    ]"
                        + "  }"
                        + "}}");
        // @formatter:on

        Config.setConfig(storage.load("config", Config.class));

        ExceptionManager.getInstance().reset();

        tstEnv = new TstEnv();
    }

    @Override
    protected void tearDown() throws Exception {
        Logs.getInstance().close();
    }

    public void testBasic() {

        try(WsP2P srv = WsP2P.canReceiveConnections(); WsP2P client1 = WsP2P.clientOnly(); WsP2P client2 = WsP2P.clientOnly()) {

            srv.onRecv((connectionId, msg) -> tstEnv.report("srv < " + connectionId + " : " + msg));

            client1.onRecv((connectionId, msg) -> tstEnv.report("client1 < " + connectionId + " : " + msg));
            client2.onRecv((connectionId, msg) -> tstEnv.report("client2 < " + connectionId + " : " + msg));

            srv.start();
            client1.start();
            client2.start();

            client1.newConnection("srv", "ws://localhost:2124/api", "Client1", "pwd345");
            client2.newConnection("srv", "ws://localhost:2124/api", "Client2", "pwd345");

            srv.send("Client1", "Msg do clienta 1");

            srv.send("Client2", "Message do clienta 2");

            Utils.sleep(1000);

            tstEnv.consumeEvent("client1 < srv : Msg do clienta 1");
            tstEnv.consumeEvent("client2 < srv : Message do clienta 2");

            tstEnv.assertNoMoreEvents();
        }

    }

    public void testAutosend() {

        try(WsP2P srv = WsP2P.canReceiveConnections(); WsP2P client1 = WsP2P.clientOnly(); WsP2P client2 = WsP2P.clientOnly()) {

            srv.onRecv((connectionId, msg) -> tstEnv.report("srv < " + connectionId + " : " + msg));

            client1.onRecv((connectionId, msg) -> tstEnv.report("client1 < " + connectionId + " : " + msg));
            client2.onRecv((connectionId, msg) -> tstEnv.report("client2 < " + connectionId + " : " + msg));

            srv.start();
            client1.start();
            client2.start();

            client1.newConnection("srv", "ws://localhost:2124/api", "Client1", "pwd345");
            client2.newConnection("srv", "ws://localhost:2124/api", "Client2", "pwd345");

            srv.autosend("Msg do catMain");
            srv.autosend("Message do costam");
            srv.autosend("Jeszcze cos innego");

            Utils.sleep(1000);

            tstEnv.consumeEvent("client1 < srv : Msg do catMain");
            tstEnv.consumeEvent("client2 < srv : Message do costam");
            tstEnv.consumeEvent("client1 < srv : Jeszcze cos innego");
//            tstEnv.consumeEvent("client2 < srv : msg do clienta 2");

            tstEnv.assertNoMoreEvents();
        }
    }


}
