package cryptoCantorWebSocket.pure;

import cryptoCantorWebSocket.pure.client.WsClient;
import cryptoCantorWebSocket.utils.TstEnv;
import cryptoCantorWebSocket.pure.server.WsServer;
import bittech.lib.utils.Config;
import bittech.lib.utils.Utils;
import bittech.lib.utils.exceptions.ExceptionManager;
import bittech.lib.utils.logs.Logs;
import bittech.lib.utils.storage.InjectStorage;
import bittech.lib.utils.tests.LogsTester;
import junit.framework.TestCase;
import org.junit.Assert;

import java.util.concurrent.atomic.AtomicReference;

public class WsSrvAndClientTest extends TestCase {


    public void setUp() {
        InjectStorage storage = new InjectStorage();

        // @formatter:off
        storage.inject("config",
                "{\"entries\":{"
                        + "  \"saveLogs\": false,"
                        + "  \"printLogs\": true,"
                        + "  \"wsServer\": {"
                        + "    \"port\":4317,"
                        + "    \"users\":["
                        + "      {\"login\":\"usr\", \"pwd\":\"pwd\", \"category\":\"cat1\"}"
                        + "    ]"
                        + "  }"
                        + "}}");
        // @formatter:on

        Config.setConfig(storage.load("config", Config.class));

        ExceptionManager.getInstance().reset();
    }

    @Override
    protected void tearDown() throws Exception {
        Logs.getInstance().close();
    }

    public void testSimpleSend() {
        LogsTester logsTester = new LogsTester();

        AtomicReference<String> clientToSrv = new AtomicReference<>();
        AtomicReference<String> srvToClient = new AtomicReference<>();

        try (WsServer sv = WsServer.fromConfig()) {

            sv.onMsgRecv((userName, msg) -> clientToSrv.set(msg));
            sv.start();

            try (WsClient wsClient = new WsClient("srv", "ws://localhost:4317/api", "usr", "pwd")) {
                wsClient.onMsgRecv((id, msg) -> srvToClient.set(msg));
                wsClient.start();
                wsClient.waitForConnection();
                Utils.sleep(1000);
                wsClient.send("clientToSrv");
                sv.send("usr", "srvToClient");
            }
        }

        Utils.sleep(1000); //TODO: Cos lepszego

        Assert.assertEquals("clientToSrv", clientToSrv.get());
        Assert.assertEquals("srvToClient", srvToClient.get());

        logsTester.consumeLog("Web socket connected to ws://localhost:4317/api");
//        logsTester.consumeLog("Web socket client - onMessage");
        logsTester.consumeLog("Proper closed by client");

        // TODO: Uncomment maybe
//        logsTester.assertNoMoreLogs();

        Assert.assertEquals(0, ExceptionManager.getInstance().getExceptionIds().size());
    }

    public void testClientStartsFirst() {
        LogsTester logsTester = new LogsTester();

        try (WsClient wsClient = new WsClient("srv", "ws://localhost:4317/api", "usr", "pwd")) {
            wsClient.onMsgRecv((id, msg) -> System.out.println(id + ":" + msg));
            wsClient.start();

            Utils.sleep(2000);
            logsTester.consumeLog("Failed to connect to ws://localhost:4317/api");

            System.out.println("----- Podłączamy server");
            TstEnv.runServer(2000);
            System.out.println("----- Odłączamy server");
            Utils.sleep(2000);

            logsTester.consumeLog("Web socket connected to ws://localhost:4317/api");
            logsTester.consumeLog("Connection lost with ws://localhost:4317/api");

            System.out.println("----- Podłączamy server");
            TstEnv.runServer(2000);
            System.out.println("----- Odłączamy server");
            Utils.sleep(2000);

            logsTester.consumeLog("Web socket connected to ws://localhost:4317/api");
            logsTester.consumeLog("Connection lost with ws://localhost:4317/api");
        }

//        logsTester.assertNoMoreLogs(); TODO: Naprawić i odcommentować
        Assert.assertEquals(0, ExceptionManager.getInstance().getExceptionIds().size());
    }

    public void testReconnectSameClient() {

        AtomicReference<String> clientToSrv = new AtomicReference<>();
        AtomicReference<String> srvToClient = new AtomicReference<>();

        try (WsServer sv = WsServer.fromConfig()) {

            sv.onMsgRecv((userName, msg) -> clientToSrv.set(msg));
            sv.start();

            try (WsClient wsClient = new WsClient("srv", "ws://localhost:4317/api", "usr", "pwd")) {
                wsClient.onMsgRecv((id, msg) -> srvToClient.set(msg));
                wsClient.start();
                wsClient.send("clientToSrv");
                sv.send("usr", "srvToClient");
                Utils.sleep(1000);
            }
            Utils.sleep(1000);

            Assert.assertEquals("clientToSrv", clientToSrv.get());
            Assert.assertEquals("srvToClient", srvToClient.get());

            System.out.println("------------ drugi przebieg --------------");

            try (WsClient wsClient = new WsClient("srv", "ws://localhost:4317/api", "usr", "pwd")) {
                wsClient.onMsgRecv((id, msg) -> srvToClient.set(msg));
                wsClient.start();
                wsClient.send("clientToSrv2");
                sv.send("usr", "srvToClient2");
                Utils.sleep(1000);
            }
            Utils.sleep(1000);

            Assert.assertEquals("clientToSrv2", clientToSrv.get());
            Assert.assertEquals("srvToClient2", srvToClient.get());
        }
    }

    //TODO: Uncomment
//    public void testConnectCompetitionSendFromServer() {
//
//        List<String> recv = new LinkedList<>();
//
//        try (WsServer sv = WsServer.fromConfig()) {
//
//            sv.onMsgRecv((userName, msg) -> System.out.println("Srv recv: " + msg));
//            sv.start();
//
//            try (WsClient wsClient = new WsClient("srv", "ws://localhost:4317/api", "usr", "pwd")) {
//                wsClient.onMsgRecv((id, msg) -> recv.add("ws1 recv: " + msg));
//                wsClient.start();
//                sv.send("usr", "srvToClient1");
//
//                System.out.println("------------ drugi przebieg --------------");
//
//                try (WsClient wsClient2 = new WsClient("srv", "ws://localhost:4317/api", "usr", "pwd")) {
//                    wsClient2.onMsgRecv((id, msg) -> recv.add("ws1 recv: " + msg));
//                    wsClient2.start();
//                    wsClient2.send("clientToSrv");
//                    sv.send("usr", "srvToClient2");
//                }
//
//                sv.send("usr", "srvToClient3");
//
//                Utils.sleep(100);
//                Assert.assertEquals("ws1 recv: srvToClient1", recv.get(0));
//                Assert.assertEquals("ws1 recv: srvToClient2", recv.get(1));
//                Assert.assertEquals("ws1 recv: srvToClient3", recv.get(2));
//            }
//        }
//    }

    public void testConnectCompetitionSendFromClient() {

        AtomicReference<String> clientToSrv = new AtomicReference<>();
        AtomicReference<String> srvToClient = new AtomicReference<>();

        try (WsServer sv = WsServer.fromConfig()) {

            sv.onMsgRecv((userName, msg) -> clientToSrv.set(msg));
            sv.start();

            try (WsClient wsClient = new WsClient("srv", "ws://localhost:4317/api", "usr", "pwd")) {
                wsClient.onMsgRecv((id, msg) -> srvToClient.set(msg));
                wsClient.start();
                wsClient.send("clientToSrv");

                Utils.sleep(1000);
                Assert.assertEquals("clientToSrv", clientToSrv.get());

                System.out.println("------------ drugi przebieg --------------");

                try (WsClient wsClient2 = new WsClient("srv", "ws://localhost:4317/api", "usr", "pwd")) {
                    wsClient2.onMsgRecv((id, msg) -> srvToClient.set(msg));
                    wsClient2.start();
                    wsClient2.send("clientToSrv2");
                    Utils.sleep(100);
                }
                Utils.sleep(1000);

                Assert.assertEquals("clientToSrv2", clientToSrv.get());

                wsClient.send("clientToSrv3");

                Utils.sleep(1000);
                Assert.assertEquals("clientToSrv3", clientToSrv.get());
            }
        }
    }


}
