package cryptoCantorWebSocket.pure;

import cryptoCantorWebSocket.pure.server.WsServer;
import bittech.lib.utils.Config;
import bittech.lib.utils.storage.InjectStorage;
import bittech.lib.utils.tests.LogsTester;
import junit.framework.TestCase;
import org.junit.Assert;

public class WsSrvTest extends TestCase {

    public void setUp() {
        InjectStorage storage = new InjectStorage();

        // @formatter:off
        storage.inject("config",
                "{\"entries\":{"
                        + "  \"saveLogs\": false,"
                        + "  \"printLogs\": true,"
                        + "  \"wsServer\": {"
                        + "    \"port\":4317,"
                        + "    \"users\":[]"
                        + "}"
                        + "}}");
        // @formatter:on

        Config.setConfig(storage.load("config", Config.class));
    }

    public void testNoMsgConsumer() {
        try(WsServer sv = WsServer.fromConfig()) {
            sv.start();
            Assert.fail("Exception not thrown");
        } catch(Exception ex) {
            Assert.assertEquals("Failed to start server on port 4317", ex.getMessage());
            Assert.assertEquals("No message consumer. You have to call WsServer.onMsgRecv before start", ex.getCause().getMessage());
        }

        LogsTester logsTester = new LogsTester();
        logsTester.assertNoMoreLogs();
    }

}
